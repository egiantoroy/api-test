<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Customer;

class CustomerController extends Controller
{
    public function index(){
        $data = Customer::all();
        return response()->json($data);
    }

    private function getCreditFunc($in_custid){
        $result = DB::selectOne("select F_GETCUSTCREDITLIMIT(
            ".$in_custid."
        ) as value from dual");
        return $result->value;
    }

    public function get_cust_list(Request $request){
        $holding_id = $request->holding_id;
        $company_id = $request->company_id;
        $unitwork_id = $request->unitwork_id;

        $data = Customer::join('T_HOLDING','T_HOLDING.c_id','=','T_CUSTOMER.c_holding_id')
        ->join('T_COMPANY','T_COMPANY.c_id','=','T_CUSTOMER.c_company_id')
        ->join('T_UNITWORK','T_UNITWORK.c_id','=','T_CUSTOMER.c_unitwork_id')
        ->join('T_COUNTRY','T_COUNTRY.c_id','=','T_CUSTOMER.c_country_id')
        ->join('T_PROVINCE','T_PROVINCE.c_id','=','T_CUSTOMER.c_province_id')
        ->join('T_CITY','T_CITY.c_id','=','T_CUSTOMER.c_city_id')
        ->join('T_PARTNERGRP','T_PARTNERGRP.c_id','=','T_CUSTOMER.c_partnergrp_id')
        ->leftjoin('T_COA','T_COA.c_id','=','T_CUSTOMER.c_coa_id')
        ->leftjoin('USERS','USERS.id','=','T_CUSTOMER.c_marketing_id')  
        ->select('T_HOLDING.c_code as holding',
        'T_COMPANY.c_code as company',
        'T_UNITWORK.c_code as unitwork',
        'T_CUSTOMER.c_id as id',
        'T_CUSTOMER.c_code as code',
        'T_CUSTOMER.c_name as name',
        'T_CUSTOMER.c_address as address',
        'T_CUSTOMER.c_phone as phone',
        'T_CUSTOMER.c_fax as fax',
        'T_CUSTOMER.c_contactperson as contactperson',
        'T_CUSTOMER.c_contactphone as contactphone',
        'T_CUSTOMER.c_contactemail as contactemail',
        'T_CUSTOMER.c_isprior as isprior',
        'T_COUNTRY.c_name as country',
        'T_PROVINCE.c_name as province',
        'T_CITY.c_name as city',
        'T_PARTNERGRP.c_name as partnergrp',
        'USERS.username as marketing',
        'T_COA.c_name as coa',
        'T_CUSTOMER.c_shipaddress as shipaddress',
        'T_CUSTOMER.c_custgrp_id as custgrp_id',
        'T_CUSTOMER.c_status as status',
        'T_CUSTOMER.c_createdby as createdby',
        'T_CUSTOMER.c_createdon as createdon'
        )
        ->where('T_CUSTOMER.c_holding_id','=',$holding_id)
        ->where('T_CUSTOMER.c_company_id','=',$company_id)
        ->where('T_CUSTOMER.c_unitwork_id','=',$unitwork_id)
        ->get();

        // $data3[] = array();
        foreach ($data as $res) {
            $data3[] = array(
                'holding' => $res->holding,
                'company' => $res->company,
                'unitwork' => $res->unitwork,
                'id' => $res->id,
                'code' => $res->code,
                'name' => $res->name,
                'address' => $res->address,
                'phone' => $res->phone,
                'fax' => $res->fax,
                'contactperson' => $res->contactperson,
                'contactphone' => $res->contactphone,
                'contactemail' => $res->contactemail,
                'isprior' => $res->isprior,
                'country' => $res->country,
                'province' => $res->province,
                'city' => $res->city,
                'partnergrp' => $res->partnergrp,
                'creditlimit' => $this->getCreditFunc($res->id),
                'marketing' => $res->marketing,
                'coa' => $res->coa,
                'shipaddress' => $res->shipaddress,
                'custgrp_id' => $res->custgrp_id,
                'status' => $res->status,
                'createdby' => $res->createdby,
                'createdon' => $res->createdon,
            );
        }

        $data2 = array (
            'Cust_List' => $data3
        );
        return response()->json($data2);
    }

    public function get_cust_by_id(Request $request){
        $holding_id = $request->holding_id;
        $company_id = $request->company_id;
        $unitwork_id = $request->unitwork_id;
        $id = $request->cust_id;

        $data = Customer::join('T_HOLDING','T_HOLDING.c_id','=','T_CUSTOMER.c_holding_id')
        ->join('T_COMPANY','T_COMPANY.c_id','=','T_CUSTOMER.c_company_id')
        ->join('T_UNITWORK','T_UNITWORK.c_id','=','T_CUSTOMER.c_unitwork_id')
        ->join('T_COUNTRY','T_COUNTRY.c_id','=','T_CUSTOMER.c_country_id')
        ->join('T_PROVINCE','T_PROVINCE.c_id','=','T_CUSTOMER.c_province_id')
        ->join('T_CITY','T_CITY.c_id','=','T_CUSTOMER.c_city_id')
        ->join('T_PARTNERGRP','T_PARTNERGRP.c_id','=','T_CUSTOMER.c_partnergrp_id')
        ->leftjoin('T_COA','T_COA.c_id','=','T_CUSTOMER.c_coa_id')
        ->leftjoin('USERS','USERS.id','=','T_CUSTOMER.c_marketing_id')  
        ->select('T_HOLDING.c_code as holding',
        'T_COMPANY.c_code as company',
        'T_UNITWORK.c_code as unitwork',
        'T_CUSTOMER.c_id as id',
        'T_CUSTOMER.c_code as code',
        'T_CUSTOMER.c_name as name',
        'T_CUSTOMER.c_address as address',
        'T_CUSTOMER.c_phone as phone',
        'T_CUSTOMER.c_fax as fax',
        'T_CUSTOMER.c_contactperson as contactperson',
        'T_CUSTOMER.c_contactphone as contactphone',
        'T_CUSTOMER.c_contactemail as contactemail',
        'T_CUSTOMER.c_isprior as isprior',
        'T_COUNTRY.c_name as country',
        'T_PROVINCE.c_name as province',
        'T_CITY.c_name as city',
        'T_PARTNERGRP.c_name as partnergrp',
        'USERS.username as marketing',
        'T_COA.c_name as coa',
        'T_CUSTOMER.c_shipaddress as shipaddress',
        'T_CUSTOMER.c_custgrp_id as custgrp_id',
        'T_CUSTOMER.c_status as status',
        'T_CUSTOMER.c_createdby as createdby',
        'T_CUSTOMER.c_createdon as createdon'
        )
        ->where('T_CUSTOMER.c_holding_id','=',$holding_id)
        ->where('T_CUSTOMER.c_company_id','=',$company_id)
        ->where('T_CUSTOMER.c_unitwork_id','=',$unitwork_id)
        ->where('T_CUSTOMER.c_id','=',$id)
        ->get();

        // $data3[] = array();
        foreach ($data as $res) {
            $data3 = (object) array(
                'holding' => $res->holding,
                'company' => $res->company,
                'unitwork' => $res->unitwork,
                'id' => $res->id,
                'code' => $res->code,
                'name' => $res->name,
                'address' => $res->address,
                'phone' => $res->phone,
                'fax' => $res->fax,
                'contactperson' => $res->contactperson,
                'contactphone' => $res->contactphone,
                'contactemail' => $res->contactemail,
                'isprior' => $res->isprior,
                'country' => $res->country,
                'province' => $res->province,
                'city' => $res->city,
                'partnergrp' => $res->partnergrp,
                'creditlimit' => $this->getCreditFunc($res->id),
                'marketing' => $res->marketing,
                'coa' => $res->coa,
                'shipaddress' => $res->shipaddress,
                'custgrp_id' => $res->custgrp_id,
                'status' => $res->status,
                'createdby' => $res->createdby,
                'createdon' => $res->createdon,
            );
        }

        // $data2 = array (
        //     'Cust_List' => $data3
        // );
        return response()->json($data3);
    }
}
