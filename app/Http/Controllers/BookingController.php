<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\BookingOrder;
use App\Holding;
use App\Currency;
use App\Customer;
use App\PartnerGroup;
use App\Country;
use App\Province;
use App\City;
use App\ExchangeRate;
use App\Material;
use App\MaterialGroup;
use App\Unit;

class BookingController extends Controller
{
    public function index(){
        $data = BookingOrder::all();
        return response()->jbon($data);
    }

    private function getBookCode(){
        $result = DB::selectOne("select F_CREATEBOOKCODE_HOLDING(
            SYSDATE
        ) as value from dual");
        return $result->value;
    }

    private function getPayCode(){
        $result = DB::selectOne("select F_CREATEPAYCODE(
            SYSDATE
        ) as value from dual");
        return $result->value;
    }

    public function post_bo_holding(Request $request){
        $valid = BookingOrder::validate($request);
        $msg = new \stdClass();
        if ($valid->status == 1) {
            DB::beginTransaction();
            try{
                $log = [];
                $holding_id = $request->input('holding_id');
                $cust_id = $request->input('cust_id');
                $dueday = $request->input('dueday');
        
                $holdingVar = Holding::find($holding_id);
                $customerVar = Customer::where('c_id','=',$cust_id)->first();
                $currencyVar = Currency::find($customerVar->c_currency_id);
                $partnerGrpVar = PartnerGroup::find($customerVar->c_partnergrp_id);
                $custCityVar = City::find($customerVar->c_city_id);
                $custProvinceVar = Province::find($customerVar->c_province_id);
                $custCountryVar = Country::find($customerVar->c_country_id);

                if($customerVar->c_currency_id) {
                    $exchangeRateVar = ExchangeRate::where('c_currency_id','=',$currencyVar->c_id)->first();
        
                    $date = date("Y-m-d 00:00:00");
                    $duedate = date('Y-m-d 00:00:00', strtotime($date. ' + '.$dueday.'days'));
                    // $duedate = date_format(date_add(date_create($date), date_interval_create_from_date_string($dueday)), "Y-m-d 00:00:00");
                    $year = date("Y");
                    $month = date("m");
            
                    //DETAIL
                    $quantity = $request->input('quantity');
                    $price = $request->input('price');
                    $taxperc = $request->input('taxperc');
                    $discperc = $request->input('discperc');
                    $remark = $request->input('remark');
                    $material_id = $request->input('material_id');
                    $bookcode = $this->getBookCode();
                    $paycode =  $this->getPayCode();

                    for($i=0;$i<count($material_id);$i++) {
                        $bo = new BookingOrder;
                        $bo->c_holding_id = $holding_id;
                        $bo->c_holding_code = $holdingVar->c_code;
                        $bo->c_holding_name = $holdingVar->c_name;
                        $bo->c_date = $date;
                        $bo->c_dueday = $dueday;
                        $bo->c_duedate = $duedate;
                        $bo->c_year = $year;
                        $bo->c_month = $month;
                        $bo->c_currency_id = $currencyVar->c_id;
                        $bo->c_currency_code = $currencyVar->c_code;
                        $bo->c_currency_name = $currencyVar->c_name;
                        $bo->c_customer_id = $cust_id;
                        $bo->c_customer_code = $customerVar->c_code;
                        $bo->c_customer_name = $customerVar->c_name;
                        $bo->c_customer_address = $customerVar->c_address;
                        $bo->c_customer_phone = $customerVar->c_phone;
                        $bo->c_customer_contactemail = $customerVar->c_contactemail;
                        $bo->c_customer_isprior = $customerVar->c_isprior;
                        $bo->c_partnergrp_id = $partnerGrpVar->c_id;
                        $bo->c_partnergrp_code = $partnerGrpVar->c_code; 
                        $bo->c_partnergrp_name = $partnerGrpVar->c_name;
                        $bo->c_country_id = $custCountryVar->c_id;
                        $bo->c_country_code = $custCountryVar->c_code;
                        $bo->c_country_name = $custCountryVar->c_name;
                        $bo->c_province_id = $custProvinceVar->c_id;
                        $bo->c_province_code = $custProvinceVar->c_code;
                        $bo->c_province_name = $custProvinceVar->c_name;
                        $bo->c_city_id = $custCityVar->c_id;
                        $bo->c_city_code = $custCityVar->c_code;
                        $bo->c_city_name = $custCityVar->c_name;
                        $bo->c_seq = $i+1;
                        $bo->c_createdby = $request->input('apiuserid');
                        $bo->c_createdshift = $request->input('apishiftid');
                        $bo->c_createdon = date("Y-m-d H:i:s");
                        $bo->c_botype = "H";
                        $bo->c_bookcode = $bookcode;
                        $bo->c_paycode = $paycode;
            
                        $materialVar = Material::find($material_id[$i]);
                        $materialGrpVar = MaterialGroup::find($materialVar->c_materialgrp_id);
                        $unitVar = Unit::find($materialVar->c_unit_id);
            
                        $bo->c_materialgrp_id = $materialGrpVar->c_id;
                        $bo->c_materialgrp_code = $materialGrpVar->c_code;
                        $bo->c_materialgrp_name = $materialGrpVar->c_name;
                        $bo->c_materialgrp_isservice = $materialGrpVar->c_isservice;
                        $bo->c_material_id = $materialVar->c_id;
                        $bo->c_material_code = $materialVar->c_code;
                        $bo->c_material_name = $materialVar->c_name;
                        $bo->c_unit_id = $unitVar->c_id;
                        $bo->c_unit_code = $unitVar->c_code;
                        $bo->c_unit_name = $unitVar->c_name;
                        $bo->c_quantity = $quantity[$i];
                        $bo->c_exchangerate = $exchangeRateVar->c_rate;
                        $bo->c_price = $price[$i];
            
                        $priceidr = $price[$i] * $exchangeRateVar->c_rate;
                        $subtotal = $price[$i] * $quantity[$i];
                        $subtotalidr = $priceidr * $quantity[$i];
                        $taxamount = $subtotal * $taxperc[$i] / 100;
                        $taxamountidr = $subtotalidr * $taxperc[$i] / 100;
                        $discamount = $subtotal * $discperc[$i] / 100;
                        $discamountidr = $subtotalidr * $discperc[$i] / 100;
                        $netamount = $subtotal + $taxamount - $discamount;
                        $netamountidr = $subtotalidr + $taxamountidr - $discamountidr;
                        
                        
                        $data_detail[] = array(
                            'material_id' => $material_id[$i],
                            'materialgrp_code' => $materialGrpVar->c_code,
                            'materialgrp_name' => $materialGrpVar->c_name,
                            'materialgrp_isservice' => $materialGrpVar->c_isservice,
                            'material_code' => $materialVar->c_code,
                            'material_name' => $materialVar->c_name,
                            'unit_id' => $unitVar->c_id,
                            'unit_code' => $unitVar->c_code,
                            'unit_name' => $unitVar->c_name,
                            'quantity' => $quantity[$i],
                            'exchangerate' => $exchangeRateVar->c_rate,
                            'price' => $price[$i],
                            'priceidr' => $priceidr,
                            'subtotal' => $subtotal,
                            'subtotalidr' => $subtotalidr,
                            'taxperc' => $taxperc[$i],
                            'taxamount' => $taxamount,
                            'taxamountidr' => $taxamountidr,
                            'discperc' => $discperc[$i],
                            'discamount' => $discamount,
                            'discamountidr' => $discamountidr,
                            'remark' => $remark[$i]
                        );
    
                        $bo->c_priceidr = $priceidr;
                        $bo->c_subtotal = $subtotal;
                        $bo->c_subtotalidr = $subtotalidr;
                        $bo->c_taxperc = $taxperc[$i];
                        $bo->c_taxamount = $taxamount;
                        $bo->c_taxamountidr = $taxamountidr;
                        $bo->c_discperc = $discperc[$i];
                        $bo->c_discamount = $discamount;
                        $bo->c_discamountidr = $discamountidr;
                        $bo->c_netamount = $subtotal + $taxamount - $discamount;
                        $bo->c_netamountidr = $subtotalidr + $taxamountidr - $discamountidr;
                        $bo->c_remark = $remark[$i];
                        $bo->save();
                        $log[] = $bo;
                }
                DB::commit();
                $msg->status = 1;
                    
                $data = array(
                    'code' => '000',
                    'messages' => 'Success',
                    'holding_id' => $holding_id, 
                    'holding_code' => $holdingVar->c_code,
                    'holding_name' => $holdingVar->c_name,
                    'date' => $date,
                    'dueday' => $dueday,
                    'duedate' => $duedate,
                    'currency_id' => $currencyVar->c_id,
                    'currency_code' => $currencyVar->c_code,
                    'currency_name' => $currencyVar->c_name,
                    'cust_id' => $cust_id,
                    'cust_code' => $customerVar->c_code,
                    'cust_name' => $customerVar->c_name,
                    'cust_address' => $customerVar->c_address,
                    'cust_phone' => $customerVar->c_phone,
                    'cust_contactemail' => $customerVar->c_contactemail,
                    'cust_isprior' => $customerVar->c_isprior,
                    'created_by' => $request->apiuserid,
                    'createdon' => $date,
                    'botype' => 'H',
                    'bookcode' => $bookcode,
                    'paycode' => $paycode,
                    'detail' => $data_detail
                );
                } else {
                    $data = array(
                        'code' => '001',
                        'messages' => 'Customer currency not set'
                    );
                }
        
            }
            catch(Exception $e){
                DB::rollback();
                $msg->sts = 0;
                $msg->msg = $e->getMessage();
            }
        } else {
            $data = array(
                // 'status' => $valid->status,
                'code' => $valid->status_res,
                'messages' => $valid->msg
            );
        }

        return response()->json($data);
    }


}
