<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class Customer extends Eloquent
{
    public $table = 'T_CUSTOMER';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;
}
