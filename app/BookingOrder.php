<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Laravel\Lumen\Auth\Authorizable;
use Yajra\Oci8\Eloquent\OracleEloquent as Eloquent;

class BookingOrder extends Eloquent
{
    public $table = 'T_BO';
    public $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;

    public static function validateHeader(Request $request){
        
        return Validator::make(
            // Input
            array(
                'Due Day' => trim($request->input('dueday')),
                'Holding' => trim($request->input('holding_id')),
                'Api User' => trim($request->input('apiuserid')),
                'Customer' => trim($request->input('cust_id'))
            ),
            // Rules
            array(
                'Due Day' => 'required|integer',
                'Customer' => 'required',
                'Holding' => 'required',
                'Api User' => 'required'
            ),
            // Message
            array(
                'required' => ':attribute is required',
                'integer' => ':attribute can only contain an integer',
                // 'regex' => ':attribute can only contain an alphabets or numbers',
                'numeric' => ':attribute can only contain a numbers',
                'date' => ':attribute is not valid date'
            )
        );
    }
    public static function validate(Request $request){
        $result = new \stdClass();
        // CEK HEADER
        $strMsg = '';
        if($request->input('apiuserid')) {
            if($request->input('apishiftid')) {
                $headerValid = self::validateHeader($request);
                if($headerValid->fails()){
                    $messages = $headerValid->messages();
                    // $strMsg = '';
                    foreach ($messages->all() as $message)
                       $strMsg = $strMsg . $message . ' | ';
                    $result->statusHeader = 0;
                    $result->msgHeader = $strMsg;  
                }
                else{
                    $result->statusHeader = 1;
                    $result->msgHeader = "Valid Header | ";
                }
            } else {
                $result->statusHeader = 0;
                $result->status_res = '003';
                $result->msgHeader = ' Default parameter for shift id not set | '; 
            }
        } else {
            $result->statusHeader = 0;
            $result->status_res = '002';
            $result->msgHeader = $result->msgHeader = ' Default parameter for user id not set | '; 
        }


        
        // CEK DETAIL
        if(intval(count($request->input('material_id'))) > 0){
            $isValid = true;
            $strMsgDet = '';
            for($i=0; $i<count($request->input('material_id')); $i++) {
                if($request->input('material_id')[$i] == '') {
                    $status_val = '004';
                    $strMsgDet = $strMsgDet . 'Material ID '. ($i+1) .' Not Found' . ' | ';
                    $isValid = false;
                }
                if($request->input('quantity')[$i] == '') {
                    $status_val = '005';
                    $strMsgDet = $strMsgDet . 'Quantity Row '. ($i+1) .' Not Found' . ' | ';
                    $isValid = false;
                }
                if($request->input('price')[$i] == '') {
                    $status_val = '005';
                    $strMsgDet = $strMsgDet . 'Price Row '. ($i+1) .' Not Found' . ' | ';
                    $isValid = false;
                }
                if($request->input('taxperc')[$i] == '') {
                    $status_val = '005';
                    $strMsgDet = $strMsgDet . 'Tax Perc Row '. ($i+1) .' Not Found' . ' | ';
                    $isValid = false;
                }
                if($request->input('discperc')[$i] == '') {
                    $status_val = '005';
                    $strMsgDet = $strMsgDet . 'Disc Perc Row '. ($i+1) .' Required' . ' | ';
                    $isValid = false;
                }               
            }
            if($isValid){
                $result->statusDetail = 1;
                $result->msgDetail = "Valid Detail";  
            }
            else{
                $result->statusDetail = 0;
                $result->status_res = $status_val;
                $result->msgDetail = $strMsgDet;  
            }
        }
        else{
            $result->statusDetail = 0;
            $result->msgDetail = 'Booking order details can not be empty !';
        }
        if($result->statusHeader == 1 && $result->statusDetail == 1){
           $result->status = 1;
           $result->msg = "Valid";
        }
        else{
           $result->status = 0;
           $result->msg = $result->msgHeader . $result->msgDetail;
        }
        return $result;
    }

}
