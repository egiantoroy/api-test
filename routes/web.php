<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->get('/customer', 'CustomerController@index');
$router->get('/admin/get_cust_list', 'CustomerController@get_cust_list');
$router->get('/admin/get_cust_by_id', 'CustomerController@get_cust_by_id');
$router->post('/post_booking_order', 'BookingController@post_bo_holding');
